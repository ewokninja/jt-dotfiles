
# Note Taking Workflow
## How to make this work:

1. Link `notetaker`, `mostRecentNote` and `buildNote` to somewhere in your path.
2. Hotkey bind to launch a terminal running `notetaker` - I use `alacritty -t
   notetaker_window -e /home/jt/.local/bin/notetaker`
3. Hotkey bind to launch zathura with the most recent note by calling the
   `mostRecentNote` script.

## Requirements

- vim
- pandoc
- xelatex
- alacritty

