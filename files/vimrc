" JT's VIM RC File
" Last updated: 20210111

" Setup vim-plug
call plug#begin('~/.vim/plugged')

" Declare our list of plugins
Plug 'tpope/vim-fugitive'
Plug 'itchyny/lightline.vim'
Plug 'junegunn/goyo.vim'
Plug 'junegunn/limelight.vim'
Plug 'preservim/nerdtree'
Plug 'airblade/vim-gitgutter'
Plug 'arcticicestudio/nord-vim'

" Initialize plugin system
call plug#end()

" Let's not try to be VI compatible
set nocompatible

" Setup our colour scheme
hi Normal guibg=NONE ctermbg=NONE

" Turn on syntax highlighting
syntax enable

" Show line numbers
set number relativenumber

" Change the leader key from \ to space
nnoremap <SPACE> <Nop>
let mapleader=" "

" Show file stats
set ruler

" Toggle relative line numbers and regular line numbers.
nmap <F6> :set invrelativenumber<CR>

" Blink cursor instead of beeping (grr)
set visualbell

" Encoding
set encoding=utf-8

" Generic Settings
set nowrap
set colorcolumn=80
highlight ColorColumn ctermbg=1 guibg=red
set signcolumn=yes
set formatoptions=tcqrn1
set tabstop=2
set shiftwidth=2
set softtabstop=2
set expandtab
set smarttab
set noshiftround
set cursorline

" Status Bar
set laststatus=2

" Last Line
set showmode
set showcmd

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Handling splits and tabs
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Horizontal split using `:sp` and vertical split using `:vsp`

set splitbelow splitright
set path+=**                " Searches current directory recursively
set wildmenu                " Display _all_ matches when _tab_ complete

" Remap splits navigation to just CTRL + hjkl
noremap <C-h> <C-w>h
noremap <C-j> <C-w>j
noremap <C-k> <C-w>k
noremap <C-l> <C-w>l

" Make adjusting split sizes more friendly
noremap <silent> <C-Left> :vertical resize +3<CR>
noremap <silent> <C-Right> :vertical resize -3<CR>
noremap <silent> <C-Up> :resize +3<CR>
noremap <silent> <C-Down> :resize -3<CR>

" Change 2 splits from vertical to horizontal and vice versa
map <Leader>th <C-w>t<C-w>H
map <Leader>tk <C-w>t<C-w>K

" Removes the pipe char as split sperators
set fillchars+=vert:\


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Lightline Stuff
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:lightline = {
      \ 'active' : {
      \ 'left' : [ [ 'mode', 'paste' ],
      \            [ 'gitbranch', 'readonly', 'filename', 'modified' ] ]
      \ },
      \ 'component_function' : {
      \   'gitbranch' : 'FugitiveHead'
      \ },
\ }


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Git Fugitive Stuff
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
nmap <leader>gj :diffget //3<CR>
nmap <leader>gf :diffget //2<CR>
nmap <leader>gs :G<CR>


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" NERD Tree Stuff
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

map <C-n> :NERDTreeToggle<CR>
let NERDTreeShowHidden=1

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Goyo Stuff
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Invoke with `:Goyo` exit with `:Goyo!`

function! s:goyo_enter()
  if executable('tmux') && strlen($TMUX)
    silent !tmux set status off
    silent !tmux list-panes -F '\#F' | grep -q Z || tmux resize-pane -Z
  endif
  set noshowmode
  set noshowcmd
  set scrolloff=999
  Limelight
  " ...
endfunction

function! s:goyo_leave()
  if executable('tmux') && strlen($TMUX)
    silent !tmux set status on
    silent !tmux list-panes -F '\#F' | grep -q Z && tmux resize-pane -Z
  endif
  set showmode
  set showcmd
  set scrolloff=5
  Limelight!
  " ...
endfunction

autocmd! User GoyoEnter nested call <SID>goyo_enter()
autocmd! User GoyoLeave nested call <SID>goyo_leave()


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Limelight Stuff
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Color name (:help cterm-colors) or ANSI code
let g:limelight_conceal_ctermfg = 'gray'
let g:limelight_conceal_ctermfg = 240

