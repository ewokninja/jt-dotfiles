# This file contains functions that are sourced by .zshrc

# Override exit to handle tmux on remote hosts
exit() {
  if [[ -z $TMUX ]]; then
    builtin exit
    return
  fi

  panes=$(tmux list-panes | wc -l)
  wins=$(tmux list-windows | wc -l) 
  count=$(($panes + $wins - 1))
  if [ $count -eq 1 ]; then
    tmux detach
  else
    builtin exit
  fi
}

# Function for apt-history
function apt-history() {
  case "$1" in
    install)
      cat /var/log/dpkg.log | grep 'install '
      ;;
    upgrade|remove)
      cat /var/log/dpkg.log | grep $1
      ;;
    rollback)
      cat /var/log/dpkg.log | grep upgrade | \
          grep "$2" -A10000000 | \
          grep "$3" -B10000000 | \
          awk '{print $4"="$5}'
      ;;
    *)
      cat /var/log/dpkg.log
      ;;
  esac
}

# Function for backing up files
bu() {
	if [ -d "$HOME/logs" ]
	then
		cp "$@" "$@".backup-`date +%y%m%d`; echo "`date +%Y-%m-%d` backed up $PWD/$@" >> ~/logs/backup.log; 
	else
		cp "$@" "$@".backup-`date +%y%m%d`; echo "`date +%Y-%m-%d` backed up $PWD/$@" >> ~/.backup.log;
	fi
}

# Geolocate IP Addresses
geolocate() {
    if [ $# -eq 0 ]; then
        echo -e "No arguments specified. Usage:\necho geolocate IPADDRESS";
        return 1;
    fi
 
    curl http://api.db-ip.com/v2/free/$@
}

# Get external IP
whatsmyip() {
  dig +short myip.opendns.com @resolver1.opendns.com
}

# I'm lazy and don't want to remember all the various commands...
ex()
{
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2)  tar xjf $1    ;;
      *.tar.gz)   tar xzf $1    ;;
      *.bz2)      bunzip2 $1    ;;
      *.rar)      unrar x $1    ;;
      *.gz)       gunzip $1     ;;
      *.tar)      tar xf $1     ;;
      *.tbz2)     tar xjf $1    ;;
      *.tgz)      tar xzf $1    ;;
      *.zip)      unzip $1      ;;
      *.Z)        uncompress $1 ;;
      *.7z)       7z x $1       ;;
      *.deb)      ar x $1       ;;
      *.tar.xz)   tar xf $1     ;;
      *.tar.zst)  unzstd $1     ;;
      *)          echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file for ex()"
  fi
}
